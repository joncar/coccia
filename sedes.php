<?php $active = 'sedes'; ?>
<?php require_once('_header.php'); ?>
        <section>             
            <!-- Index Content -->
            <div class="container">
                <div class="row content">
                    <h1>Oficina Principal</h1>
                    <p>Av.  Principal de Macaracuay , Multicentro Macaracuay, <br/>
                    Piso  9 ,  Oficina  908 , Teléfono:  +58 212 256.21.16. Fax:  +58 212 256.40.64.  Caracas ,  Venezuela.</p>
                    <h1>Centros de Operaciones / Oficinas Operativas.</h1>
                    <ol>
                        <li>Calle  El  Lago , Los Magallanes de Catia /  Frente  S/E  Magallanes . Teléfono:  +58 212 893.14.54.</li>
                        <li>Zona Industrial La Yaguara,  Calle  1   con  Calle  6 al lado de la Oficina EDC ,  La  Yaguara.<br/>
                            Teléfono: +58 212 471.42.49, Caracas ,  Venezuela.</li>
                        <li>Calle  9  de  Diciembre ,  Deposito  Nro.  78 , Teléfono:  +58 212 344.33.05, Guatire, Estado Miranda, Venezuela.</li>
                        <li>Calle  Zamora , Deposito  Nro. 55, Guatire, Estado Miranda, Venezuela.</li>
                        <li>Av.  Andrés  Bello,  Edificio Parque  Residencial  Danal  M & M, Local  Nro.  12, Urb. Mariperez<br/>
                            Teléfono: +58 212 793.67.72, Caracas ,  Venezuela.</li>
                        <li>Av.  Principal de Macaracuay , Multicentro Macaracuay, Piso 1, Oficina 1-3, Caracas ,  Venezuela.</li>
                        <li>Av.  Principal de Macaracuay , Multicentro Macaracuay, Piso  4, Oficina  4-6  , Caracas , Venezuela.</li>
                        <li>Centro   Operativo de Coccia Dominicana.  </li>
                        <li>Calle José Andrés Aybar, La Espería, Teléfono:  +809 4723190,. Fax:  +809 4723277</li>
                        <li>Santo Domingo, Republica Dominicana.</li>
                    </ol>
                </div>
                <!-- End index Content -->
            </div>
        </section>
        <?php require_once '_footer.php'; ?>
    </body>
</html>