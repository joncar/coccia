<!DOCTYPE html>
<!--[if lt IE 7 ]><html lang="en-US" class="no-js ie ie6 ie-lte7 ie-lte8 ie-lte9"><![endif]-->
<!--[if IE 7 ]><html lang="en-US" class="no-js ie ie7 ie-lte7 ie-lte8 ie-lte9"><![endif]-->
<!--[if IE 8 ]><html lang="en-US" class="no-js ie ie8 ie-lte8 ie-lte9"><![endif]-->
<!--[if IE 9 ]><html lang="en-US" class="no-js ie ie9 ie-lte9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="es-ES"><!--<![endif]-->
    <head>
        <title>Grupo COCCIA</title>
        <meta charset="utf8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <script src="http://code.jquery.com/jquery-1.10.0.js"></script>		
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>                
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">        
    </head>
    <body>
        <header>
            <!--- logo and social networks --->
            <div class="container">
                <div class="col-xs-6 col-sm-2 hidden-xs">
                    <img src="images/logo.png" style="width:100%">
                </div>
                <div id="socialNetworks" class="col-xs-12 col-sm-3 col-sm-offset-7">
                    <a href="#"><i class="fa fa-facebook-square"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                    <a href="#"><i class="fa fa-pinterest"></i></a>
                    <a href="#"><i class="fa fa-google-plus"></i></a>
                    <a href="#"><i class="fa fa-linkedin"></i></a>
                </div>
            </div>
            <!--- End Logo and social networks ---->
            <!--- Menu ----->
            <div class="menubar" id="menu">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header hidden-lg hidden-sm">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#"><img src="images/logo.png" style="width:50px"></a>
                </div>
                 <div class="container container-fullwidth">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                          <!-- Collect the nav links, forms, and other content for toggling -->
                          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                  <li <?= $active=='index'?'class="active"':'' ?>><a href="index.php">INICIO</a></li>
                                  <li <?= $active=='nosotros'?'class="active"':'' ?>><a href="nosotros.php">NOSOTROS</a></li>
                                  <li <?= $active=='sedes'?'class="active"':'' ?>><a href="sedes.php">SEDES</a></li>
                                  <li <?= $active=='servicios'?'class="active"':'' ?>><a href="servicios.php">SERVICIOS</a></li>
                                  <li <?= $active=='proyectos'?'class="active"':'' ?>><a href="proyectos.php">PROYECTOS</a></li>
                                  <li <?= $active=='clientes'?'class="active"':'' ?>><a href="clientes.php">CLIENTES</a></li>
                                  <li <?= $active=='contactos'?'class="active"':'' ?>><a href="#">CONTACTOS</a></li>
                                </ul>                            
                          </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                      </nav>
                </div>
            </div>
            <!--- End Menu -->   
            <!--- Banner -->
            <div class="menubar">
                <div class="container">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active" style="background:url(images/banner.png) no-repeat; background-size:cover; height:352px; width:100%;">
                                  <img src="images/banner.png" alt="..." style="display:none;">
                              </div>
                              <div class="item" style="background:url(images/banner2.jpg) no-repeat; background-size:cover; height:352px; width:100%;">
                                <img src="images/banner2.jpg" alt="..." style="display:none;">
                              </div>
                              <div class="item" style="background:url(images/banner3.jpg) no-repeat; background-size:cover; height:352px; width:100%;">
                                <img src="images/banner3.jpg" alt="..." style="display:none;">
                              </div>
                                <div class="item" style="background:url(images/banner4.jpg) no-repeat; background-size:cover; height:352px; width:100%;">
                                <img src="images/banner4.jpg" alt="..." style="display:none;">
                              </div>
                                <div class="item" style="background:url(images/banner5.jpg) no-repeat; background-size:cover; height:352px; width:100%;">
                                <img src="images/banner5.jpg" alt="..." style="display:none;">
                              </div>
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                            </a>
                    </div>
                </div>
            </div>
            <!-- End Banner -->
        </header>