<?php $active = 'servicios'; ?>
<?php require_once('_header.php'); ?>
        <section>             
            <!-- Index Content -->
            <div class="container">
                <div class="row content">                   
                    <p align="center"><img src="images/servicios.png"></p>
                    <p>BRINDAMOS ASISTENCIA TÉCNICA QUE ABARCA DESDE PROYECTOS ESTRATÉGICOS  A  NIVEL  DE  TODA LA ORGANIZACIÓN HASTA SERVICIOS DE ASESORAMIENTO EXPERTO EN ÁREAS ESPECÍFICAS  DE LA ESPECIALIDAD   REQUERIDA  POR   LAS EMPRESAS  CONTRATANTES. LOS OBJETIVOS, ALCANCES, PROGRAMACIÓN Y MODALIDAD DE NUESTROS SERVICIOS SON DESARROLLADOS</p>
                    <p>EN FORMA CONJUNTA CON NUESTROS  CLIENTES, GARANTIZANDO ASÍ LA DETERMINACIÓN ACERTADA  DE  SUS NECESIDADES Y EL DIMENSIONAMIENTO DE LOS SERVICIOS DE CONSULTORÍA MÁS APROPIADO A CADA CASO.</p>
                </div>
                <!-- End index Content -->
            </div>
        </section>
        <?php require_once '_footer.php'; ?>
    </body>
</html>