<?php $active = 'nosotros'; ?>
<?php require_once('_header.php'); ?>
        <section>             
            <!-- Index Content -->
            <div class="container">
                <div class="row content">
                    <p>EN EL  GRUPO COCCIA TRABAJAMOS COMO EQUIPO COHESIONADO, DISCIPLINADO, EFECTIVO,
                    CREATIVO, EN PERMANENTE SUPERACIÓN, CON ACTITUD PROACTIVA  Y  CONFIANZA  EN LAS
                    RELACIONES INTERPERSONALES.</p>



                    <p>NUESTRAS ACTUACIONES SON HECHAS CON ÉTICA ORGANIZACIONAL, HONESTIDAD, RESPETO,

                    TRANSPARENCIA, INTEGRIDAD, PROFESIONALISMO, RESPONSABILIDAD  Y COMPROMISO POR 

                    LOS  RESULTADOS, PROCURANDO LA SATISFACCIÓN DE LOS REQUERIMIENTOS  DE  LOS ACTORES

                    RELACIONADOS.</p>



                    <p>ESPECIAL ÉNFASIS LE DAMOS A LA SEGURIDAD DURANTE LOS TRABAJOS QUE HACEMOS, CON  EL

                        DEBIDO TRATAMIENTO A LOS RIESGOS .</p>



                    <p>EN  LAS  RELACIONES  CON  EL ENTORNO ENTENDEMOS LA TRASCENDENCIA DE NUESTRO TRABAJO

                    EN ASUNTOS DE INTERÉS PUBLICO, QUE EXIGEN  LA  PRESTACIÓN  DE  SERVICIOS  EFICIENTES CON

                    CALIDAD, CUIDADO  DEL AMBIENTE Y SENSIBILIDAD SOCIAL.</p>
                </div>
                <!-- End index Content -->
            </div>
        </section>
        <?php require_once '_footer.php'; ?>
    </body>
</html>