<?php $active = 'nosotros'; ?>
<?php require_once('_header.php'); ?>
        <section>             
            <!-- Index Content -->
            <div class="container">
                <div class="row content">                   
                    <div class="col-xs-6 col-sm-10">
                        <p align="center"><b>Trayectoria  y  Experiencia</b></p>
                        <p>EMPRESA  CONSULTORA  VENEZOLANA  ,  FUNDADA  EN  EL  AÑO  2001  ,  HA  PRESTADO SERVICIOS  DE  CONSULTORIA  A  INSTITUCIONES  PUBLICAS  Y  PRIVADAS  </p>
                        <p><b>INVESTIGACIÓN           ESTUDIOS              PLANIFICACIÓN            DIAGNÓSTICOS</b></p>
                        <p>INVESTIGACIÓN  EN  PLANIFICACIÓN  DE  SISTEMAS  ELÉCTRICOS  DE  POTENCIA
                            FORMULACIÓN  DE  PLANES DE  EXPANSIÓN  Y  ADECUACIÓN  DE  SISTEMAS  ELÉCTRICOS
                            ESTUDIOS  ESPECIALES  DE  SISTEMAS  DE  POTENCIA        ESTUDIOS  DE  FACTIBILIDAD
                       </p>
                       <p><b>GERENCIA  DE  DESARROLLO         GESTIÓN  ESTRATÉGICA       GESTIÓN  ENERGÉTICA</b></p>
                       <p>GESTIÓN  DE  PORTAFOLIO  DE  PROYECTOS  DE  DESARROLLO
                        PLANIFICACIÓN  ESTRATÉGICA                    CONTROL  DE  GESTIÓN
                        AUTOABASTECIMIENTO  ELÉCTRICO         PLANES  DE  AHORRO  ENERGÉTICO
                        </p>
                        <p><b>ADMINISTRACIÓN  DE  DESASTRES           PLANES  DE  CONTINGENCIA</b></p>
                        <p>SISTEMA  NACIONAL  DE  GESTIÓN  DE  RIESGO  Y  PROTECCIÓN  CIVIL
                        GESTIÓN  DE  RIESGO   --     ELABORACIÓN  DE  PLANES  DE  CONTINGENCIA
                        ANÁLISIS  DE  VULNERABILIDAD  --  PLATAFORMA  DE  GESTIÓN  DE  RIESGOS
                        </p>
                        <p><b>REGULACIÓN                                      NORMALIZACIÓN</b></p>
                        <p>MARCO  REGULATORIO       /      REGLAMENTO  DE  SERVICIO  ELÉCTRICO
                        ELABORACIÓN  DE  NORMAS  TÉCNICAS
                        CÓDIGO  ELÉCTRICO  NACIONAL
                        </p>
                        <p><b>INGENIERÍA                                                INSPECCIÓN</b></p>
                        <p>INGENIERÍA  PRE--CONCEPTUAL PROYECTOS  DE  LINEAS  DE  TRANSMISIÓN /    SUBESTACIONES INSPECCIÓN  DE  OBRAS  ELÉCTRICAS  </p>
                        <p><b>MISIÓN</b></p>
                        <p>Suministrar conocimientos experto, tecnologías competitivas y soluciones efectivas en procesos de concepción, direccionamiento, desarrollo, gestión operativa y valoración de industrias, negocios y proyectos, requeridos por instituciones y empresas que operan a nivel nacional, con quienes laboramos en condiciones de aliados confiables y logramos nuestro propósito y viabilidad como organización.</p>
                        <p><b>VISIÓN (2020)</b></p>
                        <p>Somos un equipo cohesionado de profesionales con experiencia que de ser necesario, nos complementamos eficientemente con especialistas en disciplinas requeridas para agregar valor y satisfacer a los usuarios de nuestros servicios a nivel latinoamericano. Estamos orgullosos al ser reconocidos por el elevado nivel de conocimiento, la calidad del trabajo, responsabilidad , efectividad  y actuación ética</p>
                    </div>
                     <div class="col-xs-6 col-sm-2">
                        <p><img src="images/nosotros/conselec/1.png" style="width:100%"></p>
                        <p><img src="images/nosotros/conselec/2.png" style="width:100%"></p>
                        <p><img src="images/nosotros/conselec/3.png" style="width:100%"></p>                        
                    </div>
                </div>
                <!-- End index Content -->
            </div>
        </section>
        <?php require_once '_footer.php'; ?>
    </body>
</html>