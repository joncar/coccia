<?php $active = 'nosotros'; ?>
<?php require_once('_header.php'); ?>
        <section>             
            <!-- Index Content -->
            <div class="container">
                <div class="row content">
                    <p>Electrificaciones COCCIA, fundada en 1972, cuenta con una larga experiencia de 35 años, en el mercado de 
                    Construcción, Operación y Mantenimiento de redes y sub estaciones de  transmisión y distribución de Energía Eléctrica 
                    desde 230kV hasta 120 V. De igual manera, ha construido estaciones de comunicaciones para empresas de servicio 
                    telefónicos.</p>

                    <h1>Trayectoria  y  Experiencia</h1>

                    <p><b>CONSTRUCCIÓN    DE  LINEAS  DE  TRANSMISIÓN  Y SUBESTACIONES DE DISTRIBUCIÓN ELÉCTRICA</b></p>
                    <p>Desde al año 1972, se han ejecutado construcciones de líneas de 230kV, 115kV, 69kV y 34,5kV para  Empresas del sector Eléctrico en Venezuela, como son: 
                        Electricidad de Caracas y CADAFE. Para  ello, se cuenta con herramientas, equipos y personal altamente calificados por el mercado nacional.</p>

                    <p><b>MANTENIMIENTO  DE  LINEAS  DE  TRANSMISIÓN Y SUBESTACIONES DE DISTRIBUCIÓN ELÉCTRICA</b></p>
                    <p>Al igual que en la construcción, se han ejecutado Mantenimiento de líneas de 230kV, 115kV, 69kV y 34,5kV  para empresas del sector Eléctrico en Venezuela, 
                        incluyendo las líneas de 34,5 Kv que alimentan las estaciones de bombeo de Taguaza, Sistema Fajardo y la de Puerto Maya del sistema Litoral de Hidrocapital.</p>

                    <p><b>OPERACIÓN Y MANTENIMIENTO  DE  REDES DE DISTRIBUCIÓN ELÉCTRICA</b></p>
                    <p>Se cuenta con personal y equipos técnicos especializado, 17 cuadrillas,  para operar(24 horas) y  mantener la red de Distribución Eléctrica de la C.A. Electricidad 
                    de Caracas, Zona Metropolitana y  Guarenas Guatire</p>
                    <p><b>TENDIDO  DE  CABLES  TELEFÓNICO  Y  FIBRA  ÓPTICA</b></p>
                    <p>Se ha realizado la instalación de Cables telefónicos por el método de halado y soplado en la red subterránea de la Electricidad de Caracas. Se han tendido 
                    cables ADSS y Fibra óptica sobre estructuras de Líneas de Transmisión en el territorio Nacional. Se han construido y adecuado Estaciones Radio Bases  para la 
                    ampliación de  telefonía móvil celular. Se han atendido y mantenido la red de telefonía publica de la Gran Caracas.</p>
                    <p><b>Mantenimiento de Estaciones de bombeo y transformadores de Potencia en Sub estaciones</b></p>
                    <p>Se ejecuto un diagnóstico a las estaciones de bombeo de las estaciones pertenecientes al Sistema Losada a  fin de ejecutar un mantenimiento preventivo a las mismas. Al igual que el 
                        reemplazo de transformadores de Potencia que alimentan sub. estaciones de los sistemas Losada, Fajardo y Metropolitano de Hidrocapital</p>
                    
                    
                    
                    
                    <p><b>MISIÓN</b></p>
                    <p>Satisfacer las necesidades de mantenimiento y desarrollo de proyectos en el área de sistemas eléctricos, electromecánicos y de 
comunicaciones, de empresas públicas y privadas.</p>
                    <p><b>VISIÓN (2020)</b></p>
                    <p>Consolidarse como empresa líder de servicios en las diferentes áreas en las que se desempeña. Ser reconocida como firma 
                        preferida por su calidad técnica y excelencia de personal. Ofrecer servicios en el ámbito nacional e internacional.</p>
                    <p align="center">
                        <img src="images/nosotros/coccia/1.png">
                        <img src="images/nosotros/coccia/2.png">
                        <img src="images/nosotros/coccia/3.png">
                    </p>
                </div>
                <!-- End index Content -->
            </div>
        </section>
        <?php require_once '_footer.php'; ?>
    </body>
</html>