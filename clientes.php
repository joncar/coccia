<?php $active = 'clientes'; ?>
<?php require_once('_header.php'); ?>
        <section>             
            <!-- Index Content -->
            <div class="container">
                <div class="row content">
                    <div class="btn-group btn-group-justified" role="group" aria-label="..." style="margin-bottom:30px;">
                            <a href="clientes.php" class="btn btn-success">Venezuela</a>
                            <a href="clientes2.php" class="btn btn-default">República Dominicana</a>
                      </div>
                    
                    <div class="row">                        
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv1.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv2.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv3.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv4.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv5.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv6.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv7.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv8.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv9.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv10.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv11.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv12.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv13.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="images/clientes/lcv14.jpg" alt="..." style="width:100%;">
                            </a>
                        </div>
                        
                      </div>
                </div>
                <!-- End index Content -->
            </div>
        </section>
        <?php require_once '_footer.php'; ?>
    </body>
</html>