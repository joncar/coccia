<?php $active = 'proyectos'; ?>
<?php require_once('_header.php'); ?>
        <section>             
            <!-- Index Content -->
            <div class="container">
                <div class="row content">                   
                    <ul>
                        <li>Investigación, Estudios, Planificación</li>
                        <li>Asesoría en Planificación</li>
                        <li>PLAN  DE  DESARROLLO  DEL SERVICIO  ELECTRICO  NACIONAL  PDSEN  2005 – 2024  ---  MENPET </li>
                        <li>PORTAFOLIO DE INVERSIONES EN LOS SISTEMAS DE DISTRIBUCIÓN DE ELECTRICIDAD 2006 – 2012.  ( PISDE 2006-2012 )  --  MENPET</li>
                        <li>PLAN  DE  DESARROLLO  DEL  SISTEMA   ELECTRICO  NACIONAL  PDSEN  2013 – 2019  ---  MPPEE </li>
                        <li>Gerencia de Desarrollo </li>
                        <li>Gestión de Portafolio de Proyectos  /  GPP  --  GERENCIA  DE  TRANSMISION  --  ELECTRICIDAD  DE  CARACAS</li>
                        <li>Regulación , Normalización</li>
                        <li>Ingeniería , Inspección, Diagnósticos, Auditorias.</li>
                        <li>Estudios  de  Factibilidad  /  ESTUDIOS  DE  FACTIBILIDAD  DE  AUTOGENERACION  --  MOVISTAR</li>
                        <li>Gestión  Estratégica  /  Plan Estratégico  /  PLAN  ESTRATEGICO  DE  LA  ELECTRICIDAD  DE  CARACAS ( 2005 )</li>
                        <li>Plan de Negocio </li>
                        <li>Control de Gestión  /  SISTEMA  DE  INDICADORES  DE  GESTION  --  MPPEE  (  DGEE )</li>
                        <li>Administración de Desastres / Gestión de Riesgos  /  CENTRO  NACIONAL  DE  PREVENCION Y ATENCION DE  DESASTRES – DNPCAD - MPPRIJ </li>
                        <li>Planes de Contingencia  /  PLAN  INTEGRAL  DE  CONTINGENCIA  PIC  (  NIVEL  NACIONAL  )  MOVISTAR</li>
                        <li>Gestión Energética Integral  /  Eficiencia Energética  /  Eficiencia  en  la  Gestión Publica</li>
                        <li>Plan de Ahorro  Energético  /  PLAN  INTEGRAL  DE  AHORRO  ENERGETICO  PINAE (  NIVEL  NACIONAL  )  MOVISTAR   </li>
                    </ul>
                </div>
                <!-- End index Content -->
            </div>
        </section>
        <?php require_once '_footer.php'; ?>
    </body>
</html>