<?php $active = 'nosotros'; ?>
<?php require_once('_header.php'); ?>
        <section>             
            <!-- Index Content -->
            <div class="container">
                <div class="row content">
                    <h1 align="center"><b>Trayectoria  y  Experiencia</b></h1>
                    <p>COCCIA Dominicana, fundada en 2003, cuenta con una experiencia de 4 años, en el mercado de Construcción, Operación y Mantenimiento de redes de distribución de Energía Eléctrica, así como manejo de la Gestión Comercial de empresas de servicio Publico que prestan servicio de electricidad.</p>
                    <p><b>OPERACIÓN Y MANTENIMIENTO  DE  REDES DE DISTRIBUCIÓN ELÉCTRICA</b></p>
                    <p>Se cuenta con personal y equipos técnicos especializado, 12 brigadas,  para operar ( 24 horas ) y mantener la red de Distribución Eléctrica de la Distribuidora Eléctrica EDESTE de Santo Domingo</p>
                    <p><b>Gestión de Servicios Técnicos</b></p>
                    <p>Actualmente, se ejecutan trabajos relacionados con el área de Comercialización, realizando inspecciones a los módulos de medición, con miras a determinar fraudes, irregularidades. De igual manera, se realiza la gestión de  suspensión y reconexión del servicio eléctrico por incumplimiento de pago por parte de los clientes. Se cuenta para esta gestión, con 15 brigadas especializadas y completamente equipadas.</p>
                    <p><b>Equipos antifraudes</b></p>
                    <p>Ejecutamos diseños, construcción e instalación, de equipos o módulos antifraudes, para edificaciones. Además, se realizan trabajos de adecuación a fin de blindar los módulos existentes y evitar el hurto de energía o  Manejo de equipos de medición de energía eléctrica. Esta gestión es realizada para la Empresa de distribución Eléctrica EDE Este</p>
                    <p><b>Perdidas de Energía Eléctrica</b></p>
                    <p>A lo largo de la Ciudad de Santo Domingo, se ubican 43 brigadas, 25 inspectores, entre otros, totalmente Equipados a fin de realizar la gestión de detección de irregularidades o fraudes del servicio Eléctrico En el área servida por la Distribuidora Eléctrica EDE Sur.</p>
                    <p><b>Lectura de Energía Eléctrica</b></p>
                    <p>COCCIA Dominicana, ejecuta labores de Lectura en los Centros de Medición, de clientes pertenecientes A la zona de la distribuidora eléctrica EDESur, en Santo Domingo. Para ello, cuenta con 20 lectores.</p>
                    <p><b>MISIÓN</b></p>
                    <p>SATISFACER LAS NECESIDADES  DE COMERCIALIZACIÓN, OPERACIÓN,
                            MANTENIMIENTO  Y DESARROLLO DE PROYECTOS EN EL ÁREA DE SISTEMAS 
                            ELÉCTRICOS, ELECTROMECÁNICOS Y DE COMUNICACIONES, DE EMPRESAS 
                            PÚBLICAS Y PRIVADAS DE LA REGIÓN.</p>
                    <p><b>VISIÓN</b></p>
                    <p>CONSOLIDARSE COMO EMPRESA LÍDER DE SERVICIOS EN LAS DIFERENTES ÁREAS EN 
                        LAS QUE SE DESEMPEÑA. SER RECONOCIDA COMO FIRMA PREFERIDA POR SU CALIDAD 
                        TÉCNICA Y EXCELENCIA DE PERSONAL. OFRECER SERVICIOS EN EL ÁMBITO NACIONAL E 
                        INTERNACIONAL.</p>
                    <p><b>VALORES</b></p>
                    <p>TRABAJAMOS COMO EQUIPO COHESIONADO , DISCIPLINADO , EFECTIVO ,CREATIVO , EN PERMANENTE 
                        SUPERACIÓN , CON ACTITUD PROACTIVA  Y  CONFIANZA  EN LAS RELACIONES INTERPERSONALES.
                        NUESTRAS ACTUACIONES SON HECHAS CON ÉTICA ORGANIZACIONAL , HONESTIDAD , RESPETO ,
                        TRANSPARENCIA , INTEGRIDAD , PROFESIONALISMO , RESPONSABILIDAD  Y COMPROMISO POR 
                        LOS  RESULTADOS , PROCURANDO LA SATISFACCIÓN DE LOS REQUERIMIENTOS  DE  LOS ACTORES 
                        RELACIONADOS.</p>
                    <p>ESPECIAL ÉNFASIS LE DAMOS A LA SEGURIDAD DURANTE LOS TRABAJOS QUE HACEMOS , CON  EL DEBIDO TRATAMIENTO A LOS RIESGOS .
                        EN  LAS  RELACIONES  CON  EL ENTORNO ENTENDEMOS LA TRASCENDENCIA DE NUESTRO TRABAJO 
                        EN ASUNTOS DE INTERÉS PUBLICO , QUE EXIGEN  LA  PRESTACIÓN  DE  SERVICIOS  EFICIENTES CON 
                        CALIDAD , CUIDADO  DEL AMBIENTE Y SENSIBILIDAD SOCIAL.</p>
                </div>
                <!-- End index Content -->
            </div>
        </section>
        <?php require_once '_footer.php'; ?>
    </body>
</html>