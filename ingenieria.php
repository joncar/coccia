<?php $active = 'nosotros'; ?>
<?php require_once('_header.php'); ?>
        <section>             
            <!-- Index Content -->
            <div class="container">
                <div class="row content">
                    <div class="col-xs-6 col-sm-2">
                        <p><img src="images/nosotros/ingenieria/1.png" style="width:100%"></p>
                        <p><img src="images/nosotros/ingenieria/2.png" style="width:100%"></p>
                        <p><img src="images/nosotros/ingenieria/3.png" style="width:100%"></p>
                        <p><img src="images/nosotros/ingenieria/4.png" style="width:100%"></p>
                    </div>
                    <div class="col-xs-6 col-sm-10">
                        <p>R.F. Ingeniería, ha acumulado una experiencia de 24 años, dedicada al área de distribución y comercialización del Servicio de Distribución Eléctrica. Incursionando además en el área de digitalización de planos de distribución eléctrica</p>
                        <p><b>CONSTRUCCIÓN DE  REDES  DE  DISTRIBUCIÓN</b></p>
                        <p>Ha ejecutado trabajos de construcción de redes de distribución de energía eléctrica, tanto aéreas como Subterráneas en 12,47kV y 4,8 Kv para La C.A. Electricidad de Caracas en las regiones de Guarenas Guatire y el Litoral Central. </p>
                        <p><b>MANTENIMIENTO  DE  REDES  DE  DISTRIBUCIÓN</b></p>
                        <p>Al igual que la construcción, ejecutó Mantenimiento Integral a la red de Distribución eléctrica que presta Servicios a las zona de Guarenas Guatire y al Litoral Central</p>
                        <p><b>LEVANTAMIENTO  Y  DIGITALIZACIÓN  DE  REDES</b></p>
                        <p>Se ejecutó proyecto de digitalización de planos de redes de distribución de energía Eléctrica, desarrollando trabajos de levantamiento de campo de cada estructura, para ser plasmado en planos del sistema de información geográfica de la Electricidad de Caracas.</p>
                        <p><b>GESTIÓN  COMERCIAL</b></p>
                        <p>Se ha prestado servicios en el área comercial, a la C.A. Electricidad de Caracas, acumulando una experiencia de 7 años; ejecutando labores de Lectura, Notificación, Instalaciones, Corte y Reconexiones del Servicio Eléctrico. Actualmente, cuenta con 70 trabajadores y equipos Especializados para atender alrededor de 300.000 clientes de la Región Centro de la EDC.</p>
                        <p><b>OBRAS  CIVILES</b></p>
                        <p>Ha ejecutado trabajos de adecuación, modificación y construcción de obras civiles, que sirven de soporte A las estructuras de la red de distribución, como son las tanquillas y sótanos de distribución.</p>
                        <p><b>MISIÓN</b></p>                        
                            <ul>
                                <li>CAPACIDAD DE RESPUESTA ADECUADA  a las necesidades de trabajo en condiciones normales y de emergencia</li>
                                <lI>Aseguramiento de calidad.</lI>
                                <li>Alta Productividad.</li>
                                <li>Organización adaptable a los requerimientos.</li>
                                <lI>PERSONAL DISPONIBLE en cualquier momento en casos de contingencia.</lI>
                                <li>Cumplimiento con la normativa de SEGURIDAD INDUSTRIAL</li>
                            </ul>
                        <p><b>VISIÓN (2020)</b></p>
                        <p>Consolidarse como empresa líder de servicios en las diferentes áreas en las que se desempeña. Ser reconocida como firma preferida por su calidad técnica y excelencia de personal. Ofrecer servicios en el ámbito nacional e internacional.</p>
                    </div>
                </div>
                <!-- End index Content -->
            </div>
        </section>
        <?php require_once '_footer.php'; ?>
    </body>
</html>