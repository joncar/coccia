<?php $active = 'index'; ?>
<?php require_once('_header.php'); ?>
        <section>             
            <!-- Index Content -->
            <div class="container">
                <div class="row content">
                        <p>Grupo Coccia tiene propósito prestar un servicio integral efectivo a empresas de servicio
                        público y privado e industrias en el ámbito latinoamericano, en todas sus actividades técnicas y
                        gerenciales, ofreciendo un soporte responsable, operativo, eficiente y de calidad, empleando la
                        experiencia, recursos y tecnologías adecuadas, conformando relaciones de trabajo sanas y
                        armoniosas.</p>

                    <div class="col-xs-12 col-sm-3">
                        <div class="thumbnail">
                            <div style="width:100%; height:200px; background: url(images/uploads/front1.jpg) no-repeat; background-size:cover;">
                                <img src="images/uploads/front1.jpg" alt="..." style="display:none;">
                            </div>
                            <div class="caption">
                              <h3 align="center">Electrificaciones “COCCIA” C.A.</h3>
                              <p>Proyectos y Construcciones Eléctricas</p>
                              <p align="right"><a href="coccia.php" class="btn btn-default" role="button">Leer Más</a></p>
                            </div>
                          </div>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <div class="thumbnail">
                            <div style="width:100%; height:200px; background: url(images/uploads/front2.jpg) no-repeat; background-size:cover;">
                                <img src="images/uploads/front2.jpg" alt="..." style="display:none;">
                            </div>
                            <div class="caption">
                              <h3 align="center">R.F. Ingeniería C.A.</h3>
                              <p>Empresa de Construcciones y Mantenimiento</p>
                              <p align="right"><a href="ingenieria.php" class="btn btn-default" role="button">Leer Más</a></p>
                            </div>
                          </div>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <div class="thumbnail">
                            <div style="width:100%; height:200px; background: url(images/uploads/front3.jpg) no-repeat; background-size:cover;">
                                <img src="images/uploads/front3.jpg" alt="..." style="display:none;">
                            </div>
                            <div class="caption">
                              <h3 align="center">CONSELEC</h3>
                              <p>Ingeniería C.A.</p>
                              <p align="right"><a href="conselec.php" class="btn btn-default" role="button">Leer Más</a></p>
                            </div>
                          </div>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <div class="thumbnail">
                            <div style="width:100%; height:200px; background: url(images/uploads/front4.jpg) no-repeat; background-size:cover;">
                                <img src="images/uploads/front4.jpg" alt="..." style="display:none;">
                            </div>
                            <div class="caption">
                              <h3 align="center">COCCIA DOMINICANA C.A.</h3>
                              <p>Proyectos y Construcciones Eléctricas</p>
                              <p align="right"><a href="dominicana.php" class="btn btn-default" role="button">Leer Más</a></p>
                            </div>
                          </div>
                    </div>
                </div>
                <!-- End index Content -->
            </div>
        </section>
        <?php require_once '_footer.php'; ?>
    </body>
</html>